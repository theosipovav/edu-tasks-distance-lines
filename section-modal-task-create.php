<?php
global $app;

?>
<div class="modal fade" id="ModalTaskCreate" tabindex="-1" aria-labelledby="ModalTaskCreateLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalTaskCreateLabel">Создание нового задания</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="FormTaskCreate" action="<?= $app->url ?>" method="POST">
                    <input type="hidden" name="user_id" value="<?= $app->user->id ?>">
                    <div class="row">
                        <div class="col-12">
                            <input type="text" name="title" class="form-control" placeholder="Заголовок">
                        </div>
                        <div class="col-12">
                            <textarea name="text" class="form-control" cols="30" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <input type="number" name="xa1" class="col-sm-2 form-control" placeholder="Ax">
                                <input type="number" name="ya1" class="col-sm-2 form-control" placeholder="Ay">
                                <input type="number" name="za1" class="col-sm-2 form-control" placeholder="Az">
                            </div>
                            <div class="form-group">
                                <input type="number" name="xb1" class="col-sm-2 form-control" placeholder="Bx">
                                <input type="number" name="yb1" class="col-sm-2 form-control" placeholder="By">
                                <input type="number" name="zb1" class="col-sm-2 form-control" placeholder="Bz">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <input type="number" name="xc1" class="col-sm-2 form-control" placeholder="Cx">
                                <input type="number" name="yc1" class="col-sm-2 form-control" placeholder="Cy">
                                <input type="number" name="zc1" class="col-sm-2 form-control" placeholder="Cz">
                            </div>
                            <div class="form-group">
                                <input type="number" name="xd1" class="col-sm-2 form-control" placeholder="Dx">
                                <input type="number" name="yd1" class="col-sm-2 form-control" placeholder="Dy">
                                <input type="number" name="zd1" class="col-sm-2 form-control" placeholder="Dz">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <input type="number" name="xa2" class="col-sm-2 form-control" placeholder="A1x">
                                <input type="number" name="ya2" class="col-sm-2 form-control" placeholder="A1y">
                                <input type="number" name="za2" class="col-sm-2 form-control" placeholder="A1z">
                            </div>
                            <div class="form-group">
                                <input type="number" name="xb2" class="col-sm-2 form-control" placeholder="B1x">
                                <input type="number" name="yb2" class="col-sm-2 form-control" placeholder="B1y">
                                <input type="number" name="zb2" class="col-sm-2 form-control" placeholder="B1z">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <input type="number" name="xc2" class="col-sm-2 form-control" placeholder="C1x">
                                <input type="number" name="yc2" class="col-sm-2 form-control" placeholder="C1y">
                                <input type="number" name="zc2" class="col-sm-2 form-control" placeholder="C1z">
                            </div>
                            <div class="form-group">
                                <input type="number" name="xd2" class="col-sm-2 form-control" placeholder="D1x">
                                <input type="number" name="yd2" class="col-sm-2 form-control" placeholder="D1y">
                                <input type="number" name="zd2" class="col-sm-2 form-control" placeholder="D1z">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <input type="number" name="xl1" class="col-sm-3 form-control" placeholder="Mx">
                                <input type="number" name="yl1" class="col-sm-3 form-control" placeholder="My">
                                <input type="number" name="zl1" class="col-sm-3 form-control" placeholder="Mz">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <input type="number" name="xl2" class="col-sm-3 form-control" placeholder="Nx">
                                <input type="number" name="yl2" class="col-sm-3 form-control" placeholder="Ny">
                                <input type="number" name="zl2" class="col-sm-3 form-control" placeholder="Nz">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <input type="number" name="xl3" class="col-sm-3 form-control" placeholder="Ex">
                                <input type="number" name="yl3" class="col-sm-3 form-control" placeholder="Ey">
                                <input type="number" name="zl3" class="col-sm-3 form-control" placeholder="Ez">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <input type="number" name="xl4" class="col-sm-3 form-control" placeholder="Fx">
                                <input type="number" name="yl4" class="col-sm-3 form-control" placeholder="Fy">
                                <input type="number" name="zl4" class="col-sm-3 form-control" placeholder="Fz">
                            </div>
                        </div>

                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="form-group">
                                <input type="text" name="res" class="col-sm-3 form-control" placeholder="Правильный ответ">
                            </div>
                        </div>

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="FormTaskCreateDefault" class="btn btn-orange text-white">Заполнить по умолчанию</button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
                <button type="submit" form="FormTaskCreate" name="action" value="form-task-create" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div>