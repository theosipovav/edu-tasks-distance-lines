<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.79.0">
    <link rel="canonical" href="/">
    <link rel="stylesheet" href="assets/lib/bootstrap-icons-1.2.1/bootstrap-icons.css">
    <link rel="stylesheet" href="assets/lib/bootstrap-5_0_0-beta1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.min.css">
    <title>Расстояние между прямыми - на основе API HTML5</title>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


</head>