<?php
global $app;
$task = $app->db->getTaskById($_GET['id']);
?>

<div class="row">
    <div class="col-12">
        <h1 class="h1"><?= $task->title ?></h1>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <p>
            <?= $task->text ?>
        </p>
    </div>
</div>


<div class="row">
    <div class="col-12">
        <form class="form-task row">
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-12">Прямая MN</div>
                    <div class="col-12 col-md-6">
                        <label for="inputPyramidH" class="col-sm-3 col-form-label">M (x,y,z)</label>
                        <input type="number" name="xl1" value="<?= $task->xL1 ?>" class="col-sm-3 form-control">
                        <input type="number" name="yl1" value="<?= $task->yL1 ?>" class="col-sm-3 form-control">
                        <input type="number" name="zl1" value="<?= $task->zL1 ?>" class="col-sm-3 form-control">
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="inputPyramidH" class="col-sm-3 col-form-label">N (x,y,z)</label>
                        <input type="number" name="xl2" value="<?= $task->xL2 ?>" class="col-sm-3 form-control">
                        <input type="number" name="yl2" value="<?= $task->yL2 ?>" class="col-sm-3 form-control">
                        <input type="number" name="zl2" value="<?= $task->zL2 ?>" class="col-sm-3 form-control">
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-12">Прямая EF</div>
                    <div class="col-12 col-md-6">
                        <label for="inputPyramidH" class="col-sm-3 col-form-label">E (x,y,z)</label>
                        <input type="number" name="xl3" value="<?= $task->xL3 ?>" class="col-sm-3 form-control">
                        <input type="number" name="yl3" value="<?= $task->yL3 ?>" class="col-sm-3 form-control">
                        <input type="number" name="zl3" value="<?= $task->zL3 ?>" class="col-sm-3 form-control">
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="inputPyramidH" class="col-sm-3 col-form-label">F (x,y,z)</label>
                        <input type="number" name="xl4" value="<?= $task->xL4 ?>" class="col-sm-3 form-control">
                        <input type="number" name="yl4" value="<?= $task->yL4 ?>" class="col-sm-3 form-control">
                        <input type="number" name="zl4" value="<?= $task->zL4  ?>" class="col-sm-3 form-control">
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>



<div class="row">
    <div class="col-md-6">
        <canvas id="canvas" style="border: none;" width="500" height="500"></canvas>
    </div>
    <div class="col-md-6">
        <form class="form-task container">
            <input type="hidden" id="inputTask" value="<?= $task->id ?>">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-4 col-form-label">A (x,y,z)</label>
                        <input type="number" name="xa1" value="<?= $task->xA1 ?>" class="col-sm-2 form-control">
                        <input type="number" name="ya1" value="<?= $task->yA1 ?>" class="col-sm-2 form-control">
                        <input type="number" name="za1" value="<?= $task->zA1 ?>" class="col-sm-2 form-control">
                    </div>
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-4 col-form-label">B (x,y,z)</label>
                        <input type="number" name="xb1" value="<?= $task->xB1 ?>" class="col-sm-2 form-control">
                        <input type="number" name="yb1" value="<?= $task->yB1 ?>" class="col-sm-2 form-control">
                        <input type="number" name="zb1" value="<?= $task->zB1 ?>" class="col-sm-2 form-control">
                    </div>
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-4 col-form-label">C (x,y,z)</label>
                        <input type="number" name="xc1" value="<?= $task->xC1 ?>" class="col-sm-2 form-control">
                        <input type="number" name="yc1" value="<?= $task->yC1 ?>" class="col-sm-2 form-control">
                        <input type="number" name="zc1" value="<?= $task->zC1 ?>" class="col-sm-2 form-control">
                    </div>
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-4 col-form-label">D (x,y,z)</label>
                        <input type="number" name="xd1" value="<?= $task->xD1 ?>" class="col-sm-2 form-control">
                        <input type="number" name="yd1" value="<?= $task->yD1 ?>" class="col-sm-2 form-control">
                        <input type="number" name="zd1" value="<?= $task->zD1 ?>" class="col-sm-2 form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-4 col-form-label">A<sub>1</sub> (x,y,z)</label>
                        <input type="number" name="xa2" value="<?= $task->xA2 ?>" class="col-sm-2 form-control">
                        <input type="number" name="ya2" value="<?= $task->yA2 ?>" class="col-sm-2 form-control">
                        <input type="number" name="za2" value="<?= $task->zA2 ?>" class="col-sm-2 form-control">
                    </div>
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-4 col-form-label">B<sub>1</sub> (x,y,z)</label>
                        <input type="number" name="xb2" value="<?= $task->xB2 ?>" class="col-sm-2 form-control">
                        <input type="number" name="yb2" value="<?= $task->yB2 ?>" class="col-sm-2 form-control">
                        <input type="number" name="zb2" value="<?= $task->zB2 ?>" class="col-sm-2 form-control">
                    </div>
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-4 col-form-label">C<sub>1</sub> (x,y,z)</label>
                        <input type="number" name="xc2" value="<?= $task->xC2 ?>" class="col-sm-2 form-control">
                        <input type="number" name="yc2" value="<?= $task->yC2 ?>" class="col-sm-2 form-control">
                        <input type="number" name="zc2" value="<?= $task->zC2 ?>" class="col-sm-2 form-control">
                    </div>
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-4 col-form-label">D<sub>1</sub> (x,y,z)</label>
                        <input type="number" name="xd2" value="<?= $task->xD2 ?>" class="col-sm-2 form-control">
                        <input type="number" name="yd2" value="<?= $task->yD2 ?>" class="col-sm-2 form-control">
                        <input type="number" name="zd2" value="<?= $task->zD2 ?>" class="col-sm-2 form-control">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php if ($app->role->id == 3) : ?>
    <hr>
    <div class="row">
        <div class="col-12">
            <form id="FormCreateResult" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
                <div class="form-group d-flex">
                    <input type="hidden" name="user_id" value="<?= $app->user->id ?>">
                    <input type="hidden" name="task_id" value="<?= $task->id ?>">
                    <input type="text" name="res" value="" class="form-control flex-grow-1" placeholder="Ваш ответ">
                    <button type="submit" name="action" value="form-create-result" class="btn btn-primary" style="width: 250px;">Добавить новый ответ</button>
                </div>
            </form>
        </div>
    </div>
<?php endif ?>