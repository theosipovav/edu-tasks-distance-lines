<?php
include_once 'core/app.php';
$app = new App();
?>
<!doctype html>
<html lang="en">
<?php include_once 'head.php' ?>
<body>
    <?php include_once 'header.php'; ?>
    <div class="container">
        <?php include_once $app->page; ?>
    </div>
    <?php include_once 'footer.php' ?>
    <?php include_once "section-modal-task-create.php" ?>
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <script src="assets/lib/bootstrap-5_0_0-beta1/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
    <?php if ($app->messageSuccess != false) : ?>
        <div class="modal fade" id="ModalMessageSuccess" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Внимание!</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <?= $app->messageSuccess ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#ModalMessageSuccess').modal('show');
            });
        </script>
    <?php endif ?>
    <?php if ($app->messageError != false) : ?>
        <div class="modal fade" id="ModalMessageError" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Внимание!</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <?= $app->messageError ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#ModalMessageError').modal('show');
            });
        </script>
    <?php endif ?>
    <?php if ($app->messageWarning != false) : ?>
        <div class="modal fade" id="ModalMessageWarning" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Внимание!</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <?= $app->messageWarning ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#ModalMessageWarning').modal('show');
            });
        </script>
    <?php endif ?>
</body>

</html>