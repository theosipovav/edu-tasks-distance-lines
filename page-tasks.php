<?php
global $app;
?>
<div class="row">
    <div class="col-12">
        <h1 class="h1">Задания</h1>
    </div>
</div>

<?php if ($app->user->role_id == 2) include_once 'section-tasks-for-teacher.php'; ?>
<?php if ($app->user->role_id == 3) include_once 'section-tasks-for-student.php'; ?>