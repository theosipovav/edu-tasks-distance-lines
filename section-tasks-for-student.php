<?php
$tasks = [];
$links = $app->db->getLinksByStudents($app->user->id);
foreach ($app->db->getTasks() as $key => $task) {
    foreach ($links as $key => $link) {
        if ($task->user_id == $link->parent_id) {
            $tasks[] = $task;
            break;
        }
    }
}

?>

<?php foreach ($tasks as $key => $task) : ?>
    <hr>
    <div class="row">
        <div class="col-md-1 d-flex justify-content-center align-items-center">
            <?= $task->id ?>
        </div>
        <div class="col-md-7 d-flex justify-content-start align-items-center">
            <?= $task->title ?>

        </div>
        <div class="col-md-2 d-flex flex-column justify-content-center align-items-center">
            <div>
                <?= $app->db->getUserById($task->user_id)->name ?>
            </div>
            <div>
                <?= $task->created_dt ?>
            </div>
        </div>
        <div class="col-md-2 d-flex justify-content-center align-items-center">
            <div class="btn-group-vertical">
                <a href="/?page=task&id=<?= $task->id ?>" class="btn btn-sm btn-secondary">Перейти</a>
            </div>
        </div>
    </div>
<?php endforeach ?>