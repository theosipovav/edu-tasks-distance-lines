<?php

/**
 * Debug function
 * d($var);
 */
function d($var, $caller = null)
{
    if (!isset($caller)) {
        $caller = array_shift(debug_backtrace(1));
    }
    echo '<code>File: ' . $caller['file'] . ' / Line: ' . $caller['line'] . '</code>';
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

/**
 * Debug function with die() after
 * dd($var);
 */
function dd($var)
{
    $caller = array_shift(debug_backtrace(1));
    d($var, $caller);
    die();
}


include_once 'db.php';

/**
 * Приложение
 */
class App
{
    public $db;
    public $session; // Переменные сессии
    public $get; // Переменные HTTP GET
    public $post; // Переменные HTTP POST
    public $user; // Текущий пользователь
    public $role; // Роль текущего пользователя
    public $messageSuccess;
    public $messageWarning;
    public $messageError;
    public $page;

    public $url;

    public $filterTask;

    /**
     * Инициализация подключения к базе данных
     */
    function  __construct()
    {
        $this->url = $_SERVER['REQUEST_URI'];
        session_start();
        $this->session = $_SESSION;
        $this->messageSuccess = false;
        $this->messageWarning = false;
        $this->messageError = false;
        $this->db = new db();
        $this->user = $this->getUserActive();
        $this->role = $this->getRoleActive();
        if (isset($_GET)) {
            $this->get = $_GET;
        } else {
            $this->get = false;
        }
        $this->page = $this->getPage();
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->post = $_POST;
            switch ($this->post['action']) {
                case 'form-login':
                    $this->login($this->post);
                    break;
                case 'form-logup':
                    $this->logup($this->post);
                    $this->user = $this->getUserActive();
                    $this->role = $this->getRoleActive();
                    break;
                case 'form-logout':
                    $this->logout();
                    break;
                case 'form-profile-update':
                    $this->updateProfile($this->post);
                    $this->user = $this->getUserActive();
                    $this->role = $this->getRoleActive();
                    break;
                case 'form-link-create':
                    $parent_id = $this->post["parent_id"];
                    $child_id = $this->post["child_id"];
                    $this->db->createLink($parent_id, $child_id);
                    break;
                case 'form-link-remove':
                    $parent_id = $this->post["parent_id"];
                    $child_id = $this->post["child_id"];
                    $this->db->removeLink($parent_id, $child_id);
                    break;
                case 'form-create-result':
                    $user_id = $this->post["user_id"];
                    $task_id = $this->post["task_id"];
                    $res = $this->post["res"];
                    $this->db->createResult($user_id, $task_id, $res);
                    break;
                case 'form-task-create':
                    $this->db->createTask($this->post);
                    break;
                case 'form-filter-result':
                    if (isset($this->post['user'])) {
                        $this->filterTask['user'] = $this->post['user'];
                    }
                    if (isset($this->post['status'])) {
                        $this->filterTask['status'] = $this->post['status'];
                    }
                    break;
                case 'form-task-update':
                    $this->db->updateTask($this->post);
                    break;
                case 'form-task-remove':
                    $id = $this->post['task_id'];

                    if ($this->db->removeTask($id) == false) {
                        $this->messageError = "Не удалось удалить задание<br>Возможно на него уже отвели ученики.";
                    }
                    break;
                default:
                    dd($this->post);
                    break;
            }
        } else {
            $this->post = false;
        }

        $this->session = $_SESSION;
        $this->user = $this->getUserActive();
        $this->role = $this->getRoleActive();
    }

    /**
     * Получить авторизированного пользователя
     * Если пользователь не авторизован возвращает FALSE
     */
    function getUserActive()
    {
        if (isset($this->session['user'])) {
            return $this->db->getUserById($this->session['user']);
        } else {
            return false;
        }
    }

    /**
     * Получить роль авторизированного пользователя
     * Если пользователь не авторизован возвращает FALSE
     */
    function getRoleActive()
    {
        if (isset($this->user->role_id)) {
            return $this->db->getRole($this->user->role_id);
        } else {
            return false;
        }
    }

    /**
     * Выполнить авторизацию пользователя
     */
    function login($data)
    {
        $username = $data["username"];
        $password = $data["password"];
        $this->user = $this->db->getUserByLoginAndPassword($username, $password);

        if ($this->user == false) {
            $this->messageWarning = "Не удалось выполнить вход в систему.";
            $this->messageWarning .= "<br>";
            $this->messageWarning .= "Возможно вы ввели неверной логин или пароль.";
        } else {
            $_SESSION['user'] = $this->user->id;
        }
    }

    /**
     * Выполнить регистрацию пользователя
     */
    function logup($data)
    {
        $username = $data["username"];
        $password = $data["password"];
        $passwordRepeat = $data["password-repeat"];
        $email = $data["email"];
        $name = $data["name"];
        $role_id = $data["role"];
        if ($password != $passwordRepeat) {
            $this->messageWarning = "Введеные пароль не совпадают";
            $this->user = false;
            $this->role = false;
            return;
        };
        if ($this->db->getUserByLoginAndPassword($username) != false) {
            $this->messageWarning = "Пользователь с таким логином уже существует!";
            $this->user = false;
            $this->role = false;
            return;
        }
        if ($this->db->createUser($username, $password, $email, $name, $role_id) == false) {
            $this->messageWarning = "Не удалось выполнить регистрацию нового пользователя";
            $this->messageWarning .= "<br>";
            $this->messageWarning .= $this->db->sError;
        }
        $this->messageSuccess = "Учетная запись успешно создалась!";
        $this->messageSuccess .= "<br>";
        $this->messageSuccess .= "Теперь вы можете войти под своим логином и паролем";
    }

    /**
     * Выполнить выход из системы
     */
    function logout()
    {
        session_unset();
        $this->user = false;
        $this->role = false;
    }

    /**
     * Обновление профиля
     */
    function updateProfile($data)
    {
        $id = $data["id"];
        $username = $data["username"];
        $password = $data["password"];
        $passwordRepeat = $data["password-repeat"];
        $email = $data["email"];
        $name = $data["name"];
        $role_id = $data["role"];
        if ($password != $passwordRepeat) {
            $this->messageWarning = "Введеные пароль не совпадают";
            $this->user = false;
            $this->role = false;
            return;
        }
        if ($this->user->username != $username) {
            if ($this->db->getUserByLoginAndPassword($username) != false) {
                $this->messageWarning = "Пользователь с таким логином уже существует!";
                $this->user = false;
                $this->role = false;
                return;
            }
        }
        if ($this->db->updateUser($id, $username, $password, $email, $name, $role_id) == false) {
            $this->messageWarning = $this->db->sError;
            $this->user = false;
            $this->role = false;
            return;
        }
        $this->messageSuccess = "Учетная запись успешно обновлена!";
    }

    /**
     * Получить страницу 
     */
    function getPage()
    {
        $filename = "page-home.php";
        if ($this->user == false) {
            return $filename;
        }
        if (!isset($this->get['page'])) {
            return $filename;
        }
        $filename = 'page-' . $this->get['page'] . '.php';
        if (!file_exists($filename)) {
            $filename = 'page-404.php';
        }
        return $filename;
    }
}
