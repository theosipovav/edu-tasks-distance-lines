<?php



class db
{
    private $db_host = "localhost";             // Наименование хоста
    private $db_name = "id15676525_mysql";      // Наименование базы данных
    private $db_user = "id15676525_admin";      // Имя пользователя базы данных
    private $db_password = "YEKu]*#S#A#ltTQ2";  // Пароль пользователя  базы данных
    private $mysqli;                            // Класс для работы с MySQL

    public $isError = false;                    // Флаг ошибки
    public $sError = "";                        // Текст ошибки

    /**
     * Инициализация подключения к базе данных
     */
    function  __construct()
    {
        $this->mysqli = new mysqli($this->db_host, $this->db_user, $this->db_password, $this->db_name);
        if ($this->mysqli->connect_errno) {
            $error = "Не удалось подключиться к MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
            $this->writeLog($error);
        }
    }


    /**
     * Получить роль по её идентификатору
     * @param id Идентификатор пользователя
     * @return object|FALSE
     */
    public function getRole($id)
    {
        $role = false;
        $cmd = sprintf("SELECT * FROM `roles` WHERE `id` = '%s';", $id);
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $role = $obj;
                break;
            }
            $res->close();
        }
        return $role;
    }

    /**
     * Получить список ролей
     */
    public function getRoles()
    {
        $roles = [];
        $cmd = "SELECT * FROM `roles`;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $roles[] = $obj;
            }
            $res->close();
        }
        return $roles;
    }


    /**
     * Получить список пользователей с соответствующей роли
     * @param id Идентификатор роли
     */
    public function getUsersByRole($role_id)
    {
        $users = [];
        $cmd = "SELECT * FROM `users` WHERE `role_id` = $role_id";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $users[] = $obj;
            }
            $res->close();
        }
        return $users;
    }

    /**
     * Создать нового пользователя 
     * @param username Логин для входа в систему
     * @param password Пароль для входа в систему
     * @param email Электронный адрес
     * @param name Полное имя пользователя
     * @param role_id Идентификатор роль пользователя
     */
    public function createUser($username, $password, $email, $name, $role_id)
    {
        $cmd = "INSERT INTO `users` (`username`, `password`, `email`, `name`, `role_id`)";
        $cmd .= " VALUES ('$username', '$password', '$email', '$name', '$role_id');";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->sError = "Возникла ошибка при создание новой учетной записи.<br>";
            $this->sError .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->sError .= $this->mysqli->connect_errno . "<br>";
                $this->sError .= $this->mysqli->connect_error . "<br>";
            }
            $this->writeLog($this->sError);
            return false;
        }
    }

    /**
     * Получить пользователя по логину (и паролю - опционально)
     * @return object|false Возвращает класс пользователя, соответствующий таблице в БД или FALSE если пользователь не найден
     */
    public function getUserByLoginAndPassword($username, $password = '')
    {
        $user = false;
        if ($password == '') {
            $cmd = "SELECT * FROM `users` WHERE `username` = '$username'";
        } else {
            $cmd = "SELECT * FROM `users` WHERE `username` = '$username' and `password` = '$password'";
        }
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $user = $obj;
                break;
            }
            $res->close();
        }
        return $user;
    }
    /**
     * Получить пользователя по идентификатору
     * @return object|false Возвращает класс пользователя, соответствующий таблице в БД или FALSE если пользователь не найден
     */
    public function getUserById($id)
    {
        $user = false;
        $cmd = "SELECT * FROM `users` WHERE `id` = $id";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $user = $obj;
                break;
            }
            $res->close();
        }
        return $user;
    }


    /**
     * Получить все задания
     */
    public function getTasks()
    {
        $tasks = [];
        $cmd = "SELECT * FROM `tasks` ORDER BY `id` DESC;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $tasks[] = $obj;
            }
            $res->close();
        }
        return $tasks;
    }

    /**
     * Получить все задания в диапазоне
     * @param limit_start Начало диапазона
     * @param limit_end Конце диапазона
     */
    public function getTasksLimit($limit_start = -1, $limit_end = -1)
    {
        $tasks = [];
        $cmd = "SELECT * FROM `tasks` ORDER BY `id` DESC LIMIT $limit_start, $limit_end;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $tasks[] = $obj;
            }
            $res->close();
        }
        return $tasks;
    }


    /**
     * Получить все задания автора
     * @param user_id Идентификатор автора
     */
    public function getTasksByUser($user_id)
    {
        $tasks = [];
        $cmd = "SELECT * FROM `tasks` WHERE `user_id` = $user_id;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $tasks[] = $obj;
            }
            $res->close();
        }
        return $tasks;
    }

    /**
     * Получить задание
     * @param id Идентификатор задания
     */
    public function getTaskById($id)
    {
        $task = false;
        $cmd = "SELECT * FROM `tasks` WHERE `id` = $id;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $task = $obj;
                break;
            }
            $res->close();
        }
        return $task;
    }

    /**
     * Получить связи ученик-учитель по учителю
     * @param parent_id Идентификатор учителя
     */
    public function getLinksByTeacher($parent_id)
    {
        $links = [];
        $cmd = "SELECT * FROM `links` WHERE `parent_id` = $parent_id;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $links[] = $obj;
            }
            $res->close();
        }
        return $links;
    }
    /**
     * Получить связи ученик-учитель по ученику
     * @param child_id Идентификатор ученика
     */
    public function getLinksByStudents($child_id)
    {
        $links = [];
        $cmd = "SELECT * FROM `links` WHERE `child_id` = $child_id;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $links[] = $obj;
            }
            $res->close();
        }
        return $links;
    }

    /**
     * Обновление учетной записи пользователя
     */
    public function updateUser($id, $username, $password, $email, $name, $role_id)
    {
        $cmd = "UPDATE `users` SET `username` = '$username', `password` = '$password', `email` = '$email', `name` = '$name', `role_id` = '$role_id' WHERE `users`.`id` = $id";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->sError = "Возникла ошибка при обновление учетной записи.<br>";
            $this->sError .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->sError .= $this->mysqli->connect_errno . "<br>";
                $this->sError .= $this->mysqli->connect_error . "<br>";
            }
            $this->writeLog($this->sError);
            return false;
        }
    }

    /**
     * Создать новую связь ученик-учитель
     * @param parent_id Идентификатор учителя
     * @param child_id Идентификатор ученика
     */
    public function createLink($parent_id, $child_id)
    {
        $cmd = "INSERT INTO `links` (`parent_id`, `child_id`) ";
        $cmd .= "VALUES ('$parent_id', '$child_id');";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->sError = "Возникла ошибка при создание новой связи ученик-учитель<br>";
            $this->sError .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->sError .= $this->mysqli->connect_errno . "<br>";
                $this->sError .= $this->mysqli->connect_error . "<br>";
            }
            $this->writeLog($this->sError);
            return false;
        }
    }
    /**
     * Удалить связи ученик-учитель
     * @param parent_id Идентификатор учителя
     * @param child_id Идентификатор ученика
     */
    public function removeLink($parent_id, $child_id)
    {
        $cmd = "DELETE FROM `links` WHERE `links`.`parent_id` = '$parent_id' AND `links`.`child_id` = '$child_id';";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->sError = "Возникла ошибка при удаление связи ученик-учитель<br>";
            $this->sError .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->sError .= $this->mysqli->connect_errno . "<br>";
                $this->sError .= $this->mysqli->connect_error . "<br>";
            }
            $this->writeLog($this->sError);
            return false;
        }
    }

    /**
     * Создать ответ пользователя
     * @param user_id Идентификатор автора ответа
     * @param task_id Идентификатор задания
     */
    public function createResult($user_id, $task_id, $res)
    {
        $cmd = "INSERT INTO `results` (`user_id`, `task_id`, `created_dt`, `res`) VALUES ('$user_id', '$task_id', current_timestamp(), '$res');";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->sError = "Возникла ошибка при создание новой связи ученик-учитель<br>";
            $this->sError .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->sError .= $this->mysqli->connect_errno . "<br>";
                $this->sError .= $this->mysqli->connect_error . "<br>";
            }
            $this->writeLog($this->sError);
            return false;
        }
    }


    /**
     * Получить все задания
     */
    public function getResults()
    {
        $results = [];
        $cmd = "SELECT * FROM `results` ORDER BY `id` DESC;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $results[] = $obj;
            }
            $res->close();
        }
        return $results;
    }

    /**
     * Создать задачу
     */
    public function createTask($data)
    {
        $text = $data['text'];
        $res = $data['res'];
        $user_id = $data['user_id'];
        $title = $data['title'];
        $xA1 = $data['xa1'];
        $yA1 = $data['ya1'];
        $zA1 = $data['za1'];
        $xB1 = $data['xb1'];
        $yB1 = $data['yb1'];
        $zB1 = $data['zb1'];
        $xC1 = $data['xc1'];
        $yC1 = $data['yc1'];
        $zC1 = $data['zc1'];
        $xD1 = $data['xd1'];
        $yD1 = $data['yd1'];
        $zD1 = $data['zd1'];
        $xA2 = $data['xa2'];
        $yA2 = $data['ya2'];
        $zA2 = $data['za2'];
        $xB2 = $data['xb2'];
        $yB2 = $data['yb2'];
        $zB2 = $data['zb2'];
        $xC2 = $data['xc2'];
        $yC2 = $data['yc2'];
        $zC2 = $data['zc2'];
        $xD2 = $data['xd2'];
        $yD2 = $data['yd2'];
        $zD2 = $data['zd2'];
        $xL1 = $data['xl1'];
        $yL1 = $data['yl1'];
        $zL1 = $data['zl1'];
        $xL2 = $data['xl2'];
        $yL2 = $data['yl2'];
        $zL2 = $data['zl2'];
        $xL3 = $data['xl3'];
        $yL3 = $data['yl3'];
        $zL3 = $data['zl3'];
        $xL4 = $data['xl4'];
        $yL4 = $data['yl4'];
        $zL4 = $data['zl4'];
        $cmd = "INSERT INTO `tasks` ( `text`, `res`, `user_id`, `created_dt`, `title`, `xA1`, `yA1`, `zA1`, `xB1`, `yB1`, `zB1`, `xC1`, `yC1`, `zC1`, `xD1`, `yD1`, `zD1`, `xA2`, `yA2`, `zA2`, `xB2`, `yB2`, `zB2`, `xC2`, `yC2`, `zC2`, `xD2`, `yD2`, `zD2`, `xL1`, `yL1`, `zL1`, `zL2`, `yL2`, `xL2`, `xL3`, `yL3`, `zL3`, `zL4`, `yL4`, `xL4`) ";
        $cmd .= "VALUES ( '$text', '$res', '$user_id', current_timestamp(), '$title', '$xA1', '$yA1', '$zA1', '$xB1', '$yB1', '$zB1', '$xC1', '$yC1', '$zC1', '$xD1', '$yD1', '$zD1', '$xA2', '$yA2', '$zA2', '$xB2', '$yB2', '$zB2', '$xC2', '$yC2', '$zC2', '$xD2', '$yD2', '$zD2', '$xL1', '$yL1', '$zL1', '$zL2', '$yL2', '$xL2', '$xL3', '$yL3', '$zL3', '$zL4', '$yL4', '$xL4');";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->sError = "Возникла ошибка при создание нового задания<br>";
            $this->sError .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->sError .= $this->mysqli->connect_errno . "<br>";
                $this->sError .= $this->mysqli->connect_error . "<br>";
            }
            $this->writeLog($this->sError);
            return false;
        }
    }


    public function updateTask($data)
    {
        $task_id = $data['task_id'];
        $text = $data['text'];
        $res = $data['res'];
        $user_id = $data['user_id'];
        $title = $data['title'];
        $xA1 = $data['xa1'];
        $yA1 = $data['ya1'];
        $zA1 = $data['za1'];
        $xB1 = $data['xb1'];
        $yB1 = $data['yb1'];
        $zB1 = $data['zb1'];
        $xC1 = $data['xc1'];
        $yC1 = $data['yc1'];
        $zC1 = $data['zc1'];
        $xD1 = $data['xd1'];
        $yD1 = $data['yd1'];
        $zD1 = $data['zd1'];
        $xA2 = $data['xa2'];
        $yA2 = $data['ya2'];
        $zA2 = $data['za2'];
        $xB2 = $data['xb2'];
        $yB2 = $data['yb2'];
        $zB2 = $data['zb2'];
        $xC2 = $data['xc2'];
        $yC2 = $data['yc2'];
        $zC2 = $data['zc2'];
        $xD2 = $data['xd2'];
        $yD2 = $data['yd2'];
        $zD2 = $data['zd2'];
        $xL1 = $data['xl1'];
        $yL1 = $data['yl1'];
        $zL1 = $data['zl1'];
        $xL2 = $data['xl2'];
        $yL2 = $data['yl2'];
        $zL2 = $data['zl2'];
        $xL3 = $data['xl3'];
        $yL3 = $data['yl3'];
        $zL3 = $data['zl3'];
        $xL4 = $data['xl4'];
        $yL4 = $data['yl4'];
        $zL4 = $data['zl4'];
        $cmd = "UPDATE `tasks` ";
        $cmd .= "SET ";
        $cmd .= "`text` = '$text', ";
        $cmd .= "`res` = '$res', ";
        $cmd .= "`user_id` = '$user_id', ";
        $cmd .= "`title` = '$title', ";
        $cmd .= "`xA1` = '$xA1', `yA1` = '$yA1', `zA1` = '$zA1', ";
        $cmd .= "`xB1` = '$xB1', `yB1` = '$yB1', `zB1` = '$zB1', ";
        $cmd .= "`xC1` = '$xC1', `yC1` = '$yC1', `zC1` = '$zC1', ";
        $cmd .= "`xD1` = '$xD1', `yD1` = '$yD1', `zD1` = '$zD1', ";
        $cmd .= "`xA2` = '$xA2', `yA2` = '$yA2', `zA2` = '$zA2', ";
        $cmd .= "`xB2` = '$xB2', `yB2` = '$yB2', `zB2` = '$zB2', ";
        $cmd .= "`xC2` = '$xC2', `yC2` = '$yC2', `zC2` = '$zC2', ";
        $cmd .= "`xD2` = '$xD2', `yD2` = '$yD2', `zD2` = '$zD2', ";
        $cmd .= "`xL1` = '$xL1', `yL1` = '$yL1', `zL1` = '$zL1', ";
        $cmd .= "`zL2` = '$zL2', `yL2` = '$yL2', `xL2` = '$xL2', ";
        $cmd .= "`xL3` = '$xL3', `yL3` = '$yL3', `zL3` = '$zL3', ";
        $cmd .= "`zL4` = '$zL4', `yL4` = '$yL4', `xL4` = '$xL4' ";
        $cmd .= "WHERE `tasks`.`id` = '$task_id';";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->sError = "Возникла ошибка при обновления задания<br>";
            $this->sError .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->sError .= $this->mysqli->connect_errno . "<br>";
                $this->sError .= $this->mysqli->connect_error . "<br>";
            }
            $this->writeLog($this->sError);
            return false;
        }
    }

    /**
     * Удалить задание
     */
    public function removeTask($id)
    {
        $cmd = "DELETE FROM tasks WHERE id=$id;";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->sError = "Возникла ошибка при удаления задания<br>";
            $this->sError .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->sError .= $this->mysqli->connect_errno . "<br>";
                $this->sError .= $this->mysqli->connect_error . "<br>";
            }
            return false;
        }
    }

    /**
     * Вывод текста ошибки
     */
    private function writeLog($message)
    {
        echo "<div><pre>" . print_r($message, true) . "</pre></div>";
        exit();
    }
}
