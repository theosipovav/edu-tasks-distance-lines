$(document).ready(function () {


    $("#FormTaskCreateDefault").click(function (e) {
        $("input[name=xa1]").val("0");
        $("input[name=ya1]").val("0");
        $("input[name=za1]").val("-1");
        $("input[name=xb1]").val("1");
        $("input[name=yb1]").val("0");
        $("input[name=zb1]").val("-1");
        $("input[name=xc1]").val("1");
        $("input[name=yc1]").val("0");
        $("input[name=zc1]").val("0");
        $("input[name=xd1]").val("0");
        $("input[name=yd1]").val("0");
        $("input[name=zd1]").val("0");
        $("input[name=xa2]").val("0");
        $("input[name=ya2]").val("1");
        $("input[name=za2]").val("-1");
        $("input[name=xb2]").val("1");
        $("input[name=yb2]").val("1");
        $("input[name=zb2]").val("-1");
        $("input[name=xc2]").val("1");
        $("input[name=yc2]").val("1");
        $("input[name=zc2]").val("0");
        $("input[name=xd2]").val("0");
        $("input[name=yd2]").val("1");
        $("input[name=zd2]").val("0");
        $("input[name=xl1]").val("2");
        $("input[name=yl1]").val("1");
        $("input[name=zl1]").val("1");
        $("input[name=xl2]").val("1");
        $("input[name=yl2]").val("-1");
        $("input[name=zl2]").val("1");
        $("input[name=xl3]").val("2");
        $("input[name=yl3]").val("1");
        $("input[name=zl3]").val("1");
        $("input[name=xl4]").val("0");
        $("input[name=yl4]").val("1");
        $("input[name=zl4]").val("-1");
        $("input[name=res]").val("2√5/5");
        $("input[name=title]").val("Найдите расстояние между прямыми B1C1 и MN.");
        $("textarea[name=text]").val("В кубе ABCDA1B1C1D1 рёбра равны 1. На продолжении отрезка A1C1 за точку C1 отмечена точка M так, что A1C1 = C1M, а на продолжении отрезка B1C за точку C отмечена точка N так, что B1C = CN.");

    });

    /**
     * Динамическая отрисовка графика при изменений параметров
     */
    $(".form-task input").change(function (e) {
        draw();
    });

    var formTask = $(".form-task");
    if (formTask.length > 0) {
        draw();
    }
});






function draw() {
    var x0 = 150;
    var y0 = 350;
    var z0 = 0;

    var zA1 = z0 + parseInt($(".form-task input[name=za1]").val()) * 40;
    var xA1 = zA1 + x0 + parseInt($(".form-task input[name=xa1]").val()) * 100;
    var yA1 = -zA1 + y0 - parseInt($(".form-task input[name=ya1]").val()) * 100;

    var zB1 = z0 + parseInt($(".form-task input[name=zb1]").val()) * 40;
    var xB1 = zB1 + x0 + parseInt($(".form-task input[name=xb1]").val()) * 100;
    var yB1 = -zB1 + y0 - parseInt($(".form-task input[name=yb1]").val()) * 100;

    var zC1 = z0 + parseInt($(".form-task input[name=zc1]").val()) * 40;
    var xC1 = zC1 + x0 + parseInt($(".form-task input[name=xc1]").val()) * 100;
    var yC1 = -zC1 + y0 - parseInt($(".form-task input[name=yc1]").val()) * 100;

    var zD1 = z0 + parseInt($(".form-task input[name=zd1]").val()) * 40;
    var xD1 = zD1 + x0 + parseInt($(".form-task input[name=xd1]").val()) * 100;
    var yD1 = -zD1 + y0 - parseInt($(".form-task input[name=yd1]").val()) * 100;

    var zA2 = -40 + z0 + parseInt($(".form-task input[name=za2]").val()) * 40;
    var xA2 = zA2 + x0 + parseInt($(".form-task input[name=xa2]").val()) * 100 + 40;
    var yA2 = -zA2 + y0 - parseInt($(".form-task input[name=ya2]").val()) * 100 - 40;

    var zB2 = -40 + z0 + parseInt($(".form-task input[name=zb2]").val()) * 40;
    var xB2 = zB2 + x0 + parseInt($(".form-task input[name=xb2]").val()) * 100 + 40;
    var yB2 = -zB2 + y0 - parseInt($(".form-task input[name=yb2]").val()) * 100 - 40;

    var zC2 = -40 + z0 + parseInt($(".form-task input[name=zc2]").val()) * 40;
    var xC2 = zC2 + x0 + parseInt($(".form-task input[name=xc2]").val()) * 100 + 40;
    var yC2 = -zC2 + y0 - parseInt($(".form-task input[name=yc2]").val()) * 100 - 40;

    var zD2 = -40 + z0 + parseInt($(".form-task input[name=zd2]").val()) * 40;
    var xD2 = zD2 + x0 + parseInt($(".form-task input[name=xd2]").val()) * 100 + 40;
    var yD2 = -zD2 + y0 - parseInt($(".form-task input[name=yd2]").val()) * 100 - 40;

    var zL1 = z0 + parseInt($("input[name=zl1]").val()) * 40;
    var xL1 = zL1 + x0 + parseInt($("input[name=xl1]").val()) * 100;
    var yL1 = -zL1 + y0 - parseInt($("input[name=yl1]").val()) * 100;

    var zL2 = z0 + parseInt($("input[name=zl2]").val()) * 40;
    var xL2 = zL2 + x0 + parseInt($("input[name=xl2]").val()) * 100;
    var yL2 = -zL2 + y0 - parseInt($("input[name=yl2]").val()) * 100;

    var zL3 = z0 + parseInt($("input[name=zl3]").val()) * 40;
    var xL3 = zL3 + x0 + parseInt($("input[name=xl3]").val()) * 100;
    var yL3 = -zL3 + y0 - parseInt($("input[name=yl3]").val()) * 100;

    var zL4 = z0 + parseInt($("input[name=zl4]").val()) * 40;
    var xL4 = zL4 + x0 + parseInt($("input[name=xl4]").val()) * 100;
    var yL4 = -zL4 + y0 - parseInt($("input[name=yl4]").val()) * 100;

    var canvas = document.getElementById("canvas");
    if (canvas.getContext) {
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = "#00F";
        ctx.font = "12pt Arial";
        // Отрисовка направляющих
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.strokeStyle = "green";
        ctx.moveTo(x0, 0);
        ctx.lineTo(x0, 500);
        ctx.stroke();
        ctx.beginPath();
        ctx.strokeStyle = "red";
        ctx.moveTo(0, y0);
        ctx.lineTo(500, y0);
        ctx.stroke();
        ctx.beginPath();
        ctx.strokeStyle = "blue";
        ctx.moveTo(x0 - 500, y0 + 500);
        ctx.lineTo(x0 + 500, y0 - 500);
        ctx.stroke();
        // Отрисовка фигуры
        ctx.beginPath();
        ctx.lineWidth = 3;
        ctx.strokeStyle = "black";
        ctx.moveTo(xA1, yA1);
        ctx.lineTo(xA2, yA2);
        ctx.moveTo(xB1, yB1);
        ctx.lineTo(xB2, yB2);
        ctx.moveTo(xC1, yC1);
        ctx.lineTo(xC2, yC2);
        ctx.moveTo(xD1, yD1);
        ctx.lineTo(xD2, yD2);
        ctx.moveTo(xA1, yA1);
        ctx.lineTo(xB1, yB1);
        ctx.moveTo(xA2, yA2);
        ctx.lineTo(xB2, yB2);
        ctx.moveTo(xB1, yB1);
        ctx.lineTo(xC1, yC1);
        ctx.moveTo(xC1, yC1);
        ctx.lineTo(xD1, yD1);
        ctx.moveTo(xC2, yC2);
        ctx.lineTo(xD2, yD2);
        ctx.moveTo(xB2, yB2);
        ctx.lineTo(xC2, yC2);
        ctx.moveTo(xA2, yA2);
        ctx.lineTo(xD2, yD2);
        ctx.moveTo(xA1, yA1);
        ctx.lineTo(xD1, yD1);

        ctx.stroke();
        ctx.fillText("A", xA1 - 20, yA1 - 5);
        ctx.fillText("B", xB1 - 20, yB1 - 5);
        ctx.fillText("C", xC1 - 20, yC1 - 5);
        ctx.fillText("D", xD1 - 20, yD1 - 5);
        ctx.fillText("A1", xA2 - 20, yA2 - 5);
        ctx.fillText("B1", xB2 - 20, yB2 - 5);
        ctx.fillText("C1", xC2 - 20, yC2 - 5);
        ctx.fillText("D1", xD2 - 20, yD2 - 5);

        // Отрисовка прямых
        ctx.beginPath();
        ctx.lineWidth = 5;
        ctx.fillStyle = "red";
        ctx.strokeStyle = "red";
        ctx.moveTo(xL1, yL1);
        ctx.lineTo(xL2, yL2);
        ctx.moveTo(xL3, yL3);
        ctx.lineTo(xL4, yL4);
        ctx.stroke();
        ctx.fillText("M", xL1 + 5, yL1 + 20);
        ctx.fillText("N", xL2 + 5, yL2 + 20);
        ctx.fillText("E", xL3 + 5, yL3 + 20);
        ctx.fillText("F", xL4 + 5, yL4 + 20);
    }
}