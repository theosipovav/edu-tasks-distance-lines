<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <div class="d-flex flex-column">

            <h1 class="display-1">ERROR 404</h1>
            <p class="lead">Страница не найдена или больше не существует</p>
            <hr>
            <div class="d-flex justify-content-center">
                <a href="/" class="btn btn-lg btn-outline-primary">Вернуться на главную</a>
            </div>

        </div>
    </div>
</div>