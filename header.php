<?php
global $app;
?>
<header>
    <div class="navbar navbar-dark bg-dark bg-light bg-teal shadow-sm">
        <div class="container">
            <nav class="navbar navbar-dark bg-primary navbar-expand-lg ">
                <div class="container-fluid">
                    <a class="navbar-brand " href="/">
                        <i class="bi bi-book me-1"></i>
                        <span>Проверь свои знания!</span>
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Главная</a>
                            </li>
                            <?php if ($app->user != false) : ?>
                                <?php if ($app->role->id == 2) include 'header-nav-teacher.php'; ?>
                                <?php if ($app->role->id == 3) include 'header-nav-student.php'; ?>
                            <?php endif ?>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="/?page=help-example">Пример решения</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <?php if ($app->user != false) : ?>
                <div class="d-flex justify-content-center align-items-center">
                    <form id="FormLogout" action="/" method="POST" style="display: none;"></form>
                    <button type="submit" form="FormLogout" class="btn btn-light btn-white me-2" name="action" value="form-logout">Выход</button>
                    <button type="button" class="btn btn-light" data-bs-toggle="modal" data-bs-target="#ModalProfile">
                        <i class="bi bi-person-lines-fill"></i>
                        <span>Профиль</span>
                    </button>

                </div>
            <?php endif ?>
        </div>
    </div>
</header>

<!-- Профиля -->
<div class="modal fade" id="ModalProfile" aria-labelledby="ModalProfileLabel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalProfileLabel"><?= $app->user->name ?></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-sm">
                    <tr>
                        <th colspan="2">
                            <span class="me-1">Профиль</span>
                            <button type="button" class="btn btn-sm btn-secondary float-end" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#ModalProfileUpdate">Редактировать</button></th>
                    </tr>
                    <tr>
                        <td>Идентификатор</td>
                        <td><?= $app->user->id ?></td>
                    </tr>
                    <tr>
                        <td>Логин для входа в систему</td>
                        <td><?= $app->user->username ?></td>
                    </tr>
                    <tr>
                        <td>Электронный адрес</td>
                        <td><?= $app->user->email ?></td>
                    </tr>
                    <tr>
                        <td>Полное имя пользователя</td>
                        <td><?= $app->user->name ?></td>
                    </tr>
                </table>
                <table class="table table-sm">
                    <tr>
                        <th colspan="2"><span>Роль</span> </th>
                    </tr>
                    <tr>
                        <td>Идентификатор</td>
                        <td><?= $app->role->id ?></td>
                    </tr>
                    <tr>
                        <td>Логин для входа в систему</td>
                        <td><?= $app->role->name ?></td>
                    </tr>
                </table>

                <?php if ($app->role->id == 2) : ?>
                    <table class="table table-sm">
                        <tr>
                            <th colspan="2"><span>Ваши ученики</span></th>
                        </tr>
                        <?php foreach ($app->db->getLinksByTeacher($app->user->id) as $key => $link) : ?>
                            <?php $user = $app->db->getUserById($link->child_id); ?>
                            <tr>
                                <td><?= $user->id ?></td>
                                <td><?= $user->name ?></td>
                            </tr>
                        <?php endforeach ?>

                    </table>
                <?php endif ?>
                <?php if ($app->role->id == 3) : ?>
                    <table class="table table-sm">
                        <tr>
                            <th colspan="2"><span>Ваши учителя</span>
                                <div class="float-end">
                                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#ModalLinkCreate">
                                        <i class="bi bi-person-plus"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#ModalLinksRemove">
                                        <i class="bi bi-person-dash"></i>
                                    </button>
                                </div>
                            </th>
                        </tr>
                        <?php foreach ($app->db->getLinksByStudents($app->user->id) as $key => $link) : ?>
                            <?php $user = $app->db->getUserById($link->parent_id); ?>
                            <tr>
                                <td><?= $user->id ?></td>
                                <td><?= $user->name ?></td>
                            </tr>
                        <?php endforeach ?>

                    </table>
                <?php endif ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<!-- Редактирование профиля -->
<div class="modal fade" id="ModalProfileUpdate" tabindex="-1" aria-labelledby="ModalProfileUpdateLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalProfileUpdateLabel">Редактирование</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="FormProfileUpdate" action="/" method="POST">
                    <div class="form-group">
                        <label for="InputFormProfileUpdateId">Идентификатор</label>
                        <input type="text" name="id" value="<?= $app->user->id ?>" class="form-control" id="InputFormProfileUpdateId" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="InputFormProfileUpdateUsername">Логин</label>
                        <input type="text" name="username" value="<?= $app->user->username ?>" class="form-control" id="InputFormProfileUpdateUsername" required title="Логин должен содержать не менее 3х символов и только латинские буквы в любом регистре ">
                    </div>
                    <div class="form-row">
                        <div class="form-group">
                            <label for="InputFormProfileUpdatePassword">Пароль</label>
                            <input type="password" name="password" value="<?= $app->user->password ?>" class="form-control" id="InputFormProfileUpdatePassword" required title="Пароль должен содержать не менее 3х символов">
                        </div>
                        <div class="form-group">
                            <label for="InputFormProfileUpdatePasswordRepeat">Повторите пароль</label>
                            <input type="password" name="password-repeat" value="<?= $app->user->password ?>" class="form-control" id="InputFormProfileUpdatePasswordRepeat" required title="Пароль должен содержать не менее 3х символов">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InputFormProfileUpdateEmail">Адрес электронной почты</label>
                        <input type="email" name="email" value="<?= $app->user->email ?>" class="form-control" id="InputFormProfileUpdateEmail" placeholder="name@mail.ru" required>
                    </div>
                    <div class="form-group">
                        <label for="inputFormLogUpName">Ваше имя</label>
                        <input type="text" class="form-control" value="<?= $app->user->name ?>" id="inputFormLogUpName" name="name" placeholder="Иванов Иван Иванович" required>
                    </div>
                    <div id="ContainerSelectFormLogUpRole" class="form-row">
                        <div class="col-12">
                            <label for="SelectFormLogUpRole">Ваша роль</label>
                            <select id="SelectFormLogUpRole" class="form-select" aria-label="Ваша учитель" name="role" required>
                                <?php foreach ($app->db->getRoles(2) as $key => $role) : ?>
                                    <?php if ($role->id == $app->role->id) : ?>
                                        <option value="<?= $role->id ?>" selected><?= $role->name ?></option>
                                    <?php else : ?>
                                        <option value="<?= $role->id ?>"><?= $role->name ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
                <button type="submit" form="FormProfileUpdate" name="action" value="form-profile-update" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div>
<!-- Редактирование связи - добавить -->
<div class="modal fade" id="ModalLinkCreate" tabindex="-1" aria-labelledby="ModalLinkCreateLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLinkCreateLabel">Добавить учителя</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="FormLinkCreate" action="/" method="POST">
                    <input type="text" name="child_id" value="<?= $app->user->id ?>" class="form-control" id="InputFormProfileUpdateId" readonly required>
                    <div class="form-group">
                        <?php
                        $teachers = [];
                        $links = $app->db->getLinksByStudents($app->user->id);
                        foreach ($app->db->getUsersByRole(2) as $key => $teacher) {
                            $isExist = false;
                            foreach ($links as $key => $link) {
                                if ($app->db->getUserById($link->parent_id)->id == $teacher->id) {
                                    $isExist = true;
                                    continue;
                                }
                            }
                            if (!$isExist) {
                                $teachers[] = $teacher;
                            }
                        }
                        ?>
                        <label for="SelectFormLogUpRole">Учителя</label>
                        <select id="SelectFormLogUpRole" class="form-select" aria-label="Ваша учитель" name="parent_id" required>
                            <?php foreach ($teachers as $key => $teacher) : ?>
                                <option value="<?= $teacher->id ?>"><?= $teacher->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
                <button type="submit" form="FormLinkCreate" name="action" value="form-link-create" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div>
<!-- Редактирование связи - удалить -->
<div class="modal fade" id="ModalLinksRemove" tabindex="-1" aria-labelledby="ModalLinksRemoveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLinksRemoveLabel">Удалить учителя</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="FormLinkRemove" action="/" method="POST">
                    <input type="text" name="child_id" value="<?= $app->user->id ?>" class="form-control" id="InputFormProfileUpdateId" readonly required>
                    <div class="form-group">
                        <label for="SelectFormLogUpRole">Учителя</label>
                        <select id="SelectFormLogUpRole" class="form-select" aria-label="Ваша учитель" name="parent_id" required>
                            <?php foreach ($app->db->getLinksByStudents($app->user->id) as $key => $link) : ?>
                                <?php $teacher = $app->db->getUserById($link->parent_id); ?>
                                <option value="<?= $teacher->id ?>"><?= $teacher->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
                <button type="submit" form="FormLinkRemove" name="action" value="form-link-remove" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div>