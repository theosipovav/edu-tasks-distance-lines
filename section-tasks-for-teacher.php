<?php
$tasks = $app->db->getTasks();


?>
<div class="row mt-5">
    <div class="col">
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ModalTaskCreate">
            Добавить новое задание
        </button>
    </div>
</div>


<?php foreach ($tasks as $key => $task) : ?>
    <hr>
    <div class="row">
        <div class="col-md-1 d-flex justify-content-center align-items-center">
            <?= $task->id ?>
        </div>
        <div class="col-md-7 d-flex justify-content-start align-items-center">
            <?= $task->title ?>

        </div>
        <div class="col-md-2 d-flex flex-column justify-content-center align-items-center">
            <div>
                <?= $app->db->getUserById($task->user_id)->name ?>
            </div>
            <div>
                <?= $task->created_dt ?>
            </div>
        </div>
        <div class="col-md-2 d-flex justify-content-center align-items-center">
            <div class="btn-group">
                <a href="/?page=task&id=<?= $task->id ?>" class="btn btn-sm btn-secondary" title="Перейти">
                    <i class="bi bi-reply-fill"></i>
                </a>
                <a href="/?page=task-update&id=<?= $task->id ?>" class="btn btn-sm btn-secondary" title="Редактировать">
                    <i class="bi bi-pencil-square"></i>
                </a>
            </div>
        </div>
    </div>
<?php endforeach ?>