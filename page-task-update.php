<?php
global $app;
$task = $app->db->getTaskById($_GET['id']);
?>
<form id="FormTaskRemove" action="/?page=tasks" method="post">
    <input type="hidden" name="task_id" value="<?= $task->id ?>">
</form>
<form id="FormTaskUpdate" action="<?= $app->url ?>" class="form-task" method="post">
    <div class="row mt-5">
        <div class="col-12 d-flex flex-column">
            <label>
                <h3 class="h3">Заголовок</h3>
                <input type="text" name="title" value="<?= $task->title ?>" class="form-control">
            </label>
        </div>
        <div class="col-12 d-flex flex-column">
            <label>
                <h3 class="h3">Текст здания</h3>
                <textarea name="text" id="" cols="30" rows="5" class="form-control"><?= $task->text ?></textarea>
            </label>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-md-6">
            <div class="row">
                <div class="col-12">
                    <h4 class="h4">Прямая MN</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <label for="inputPyramidH" class="col-sm-3 col-form-label">M (x,y,z)</label>
                    <input type="number" name="xl1" value="<?= $task->xL1 ?>" class="col-sm-3 form-control">
                    <input type="number" name="yl1" value="<?= $task->yL1 ?>" class="col-sm-3 form-control">
                    <input type="number" name="zl1" value="<?= $task->zL1 ?>" class="col-sm-3 form-control">
                </div>
                <div class="col-12 col-md-6">
                    <label for="inputPyramidH" class="col-sm-3 col-form-label">N (x,y,z)</label>
                    <input type="number" name="xl2" value="<?= $task->xL2 ?>" class="col-sm-3 form-control">
                    <input type="number" name="yl2" value="<?= $task->yL2 ?>" class="col-sm-3 form-control">
                    <input type="number" name="zl2" value="<?= $task->zL2 ?>" class="col-sm-3 form-control">
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-12">
                    <h4 class="h4">Прямая EF</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <label for="inputPyramidH" class="col-sm-3 col-form-label">E (x,y,z)</label>
                    <input type="number" name="xl3" value="<?= $task->xL3 ?>" class="col-sm-3 form-control">
                    <input type="number" name="yl3" value="<?= $task->yL3 ?>" class="col-sm-3 form-control">
                    <input type="number" name="zl3" value="<?= $task->zL3 ?>" class="col-sm-3 form-control">
                </div>
                <div class="col-12 col-md-6">
                    <label for="inputPyramidH" class="col-sm-3 col-form-label">F (x,y,z)</label>
                    <input type="number" name="xl4" value="<?= $task->xL4 ?>" class="col-sm-3 form-control">
                    <input type="number" name="yl4" value="<?= $task->yL4 ?>" class="col-sm-3 form-control">
                    <input type="number" name="zl4" value="<?= $task->zL4  ?>" class="col-sm-3 form-control">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-flex justify-content-center align-items-center">
            <canvas id="canvas" style="border: none;" width="500" height="500"></canvas>
        </div>
    </div>
    <div class="row">

        <div class="col-12 col-md-3">
            <div class="form-group">
                <label for="inputPyramidH" class="col-sm-4 col-form-label">A (x,y,z)</label>
                <input type="number" name="xa1" value="<?= $task->xA1 ?>" class="col-sm-2 form-control">
                <input type="number" name="ya1" value="<?= $task->yA1 ?>" class="col-sm-2 form-control">
                <input type="number" name="za1" value="<?= $task->zA1 ?>" class="col-sm-2 form-control">
            </div>
            <div class="form-group">
                <label for="inputPyramidH" class="col-sm-4 col-form-label">B (x,y,z)</label>
                <input type="number" name="xb1" value="<?= $task->xB1 ?>" class="col-sm-2 form-control">
                <input type="number" name="yb1" value="<?= $task->yB1 ?>" class="col-sm-2 form-control">
                <input type="number" name="zb1" value="<?= $task->zB1 ?>" class="col-sm-2 form-control">
            </div>

        </div>

        <div class="col-12 col-md-3">

            <div class="form-group">
                <label for="inputPyramidH" class="col-sm-4 col-form-label">C (x,y,z)</label>
                <input type="number" name="xc1" value="<?= $task->xC1 ?>" class="col-sm-2 form-control">
                <input type="number" name="yc1" value="<?= $task->yC1 ?>" class="col-sm-2 form-control">
                <input type="number" name="zc1" value="<?= $task->zC1 ?>" class="col-sm-2 form-control">
            </div>
            <div class="form-group">
                <label for="inputPyramidH" class="col-sm-4 col-form-label">D (x,y,z)</label>
                <input type="number" name="xd1" value="<?= $task->xD1 ?>" class="col-sm-2 form-control">
                <input type="number" name="yd1" value="<?= $task->yD1 ?>" class="col-sm-2 form-control">
                <input type="number" name="zd1" value="<?= $task->zD1 ?>" class="col-sm-2 form-control">
            </div>
        </div>
        <div class="col-12 col-md-3">
            <div class="form-group">
                <label for="inputPyramidH" class="col-sm-4 col-form-label">A<sub>1</sub> (x,y,z)</label>
                <input type="number" name="xa2" value="<?= $task->xA2 ?>" class="col-sm-2 form-control">
                <input type="number" name="ya2" value="<?= $task->yA2 ?>" class="col-sm-2 form-control">
                <input type="number" name="za2" value="<?= $task->zA2 ?>" class="col-sm-2 form-control">
            </div>
            <div class="form-group">
                <label for="inputPyramidH" class="col-sm-4 col-form-label">B<sub>1</sub> (x,y,z)</label>
                <input type="number" name="xb2" value="<?= $task->xB2 ?>" class="col-sm-2 form-control">
                <input type="number" name="yb2" value="<?= $task->yB2 ?>" class="col-sm-2 form-control">
                <input type="number" name="zb2" value="<?= $task->zB2 ?>" class="col-sm-2 form-control">
            </div>
        </div>
        <div class="col-12 col-md-3">

            <div class="form-group">
                <label for="inputPyramidH" class="col-sm-4 col-form-label">C<sub>1</sub> (x,y,z)</label>
                <input type="number" name="xc2" value="<?= $task->xC2 ?>" class="col-sm-2 form-control">
                <input type="number" name="yc2" value="<?= $task->yC2 ?>" class="col-sm-2 form-control">
                <input type="number" name="zc2" value="<?= $task->zC2 ?>" class="col-sm-2 form-control">
            </div>
            <div class="form-group">
                <label for="inputPyramidH" class="col-sm-4 col-form-label">D<sub>1</sub> (x,y,z)</label>
                <input type="number" name="xd2" value="<?= $task->xD2 ?>" class="col-sm-2 form-control">
                <input type="number" name="yd2" value="<?= $task->yD2 ?>" class="col-sm-2 form-control">
                <input type="number" name="zd2" value="<?= $task->zD2 ?>" class="col-sm-2 form-control">
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 d-flex flex-column">
            <input type="hidden" name="task_id" value="<?= $task->id ?>">
            <input type="hidden" name="user_id" value="<?= $task->user_id ?>">
            <label>
                <h4 class="h4">Ответ</h4>
                <input type="text" name="res" value="<?= $task->res ?>" class="form-control">
            </label>
            <button type="submit" form="FormTaskUpdate" name="action" value="form-task-update" class="btn btn-primary mt-1">Обновить</button>
            <button type="submit" form="FormTaskRemove" name="action" value="form-task-remove" class="btn btn-danger mt-1">Удалить</button>

        </div>
    </div>

</form>