<?php
global $app;
$tasks = [];
if ($app->role->id == 1 || $app->role->id == 2) {
    $tasks = $app->db->getTasksLimit(0, 9);
}
if ($app->role->id == 3) {
    $links = $app->db->getLinksByStudents($app->user->id);
    foreach ($app->db->getTasks() as $key => $task) {
        if (count($tasks) > 8) {
            break;
        }
        foreach ($links as $key => $link) {
            if ($task->user_id == $link->parent_id) {
                $tasks[] = $task;
                break;
            }
        }
    }
}

?>
<div class="py-5 text-center container">
    <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <h1 class="fw-light">Добрый день <span><?= $app->user->name ?></span>!</h1>
            <p class="lead text-muted">
                Вы находитесь на онлайн платформе по проверке остаточных знаний.
            </p>
            <p class="lead text-muted">
                Для продолжения работы вам необходимо выполнить вход под своими учетными данными или пройти процедуру регистрации.
            </p>
        </div>
    </div>
</div>
<div class="album py-5">
    <div class="container">
        <div class="row">
            <h2 class="h2">
                Последние задания
            </h2>
        </div>
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            <?php foreach ($tasks as $key => $task) : ?>
                <div class="col">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <p class="card-text"><?= $task->title ?></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <?php if ($app->role->id == 1 || $app->role->id == 2) : ?>
                                        <a href="/?page=task&id=<?= $task->id ?>" class="btn btn-sm btn-outline-secondary">
                                            <i class="bi bi-reply-fill"></i>
                                        </a>
                                        <a href="/?page=task-update&id=<?= $task->id ?>" class="btn btn-sm btn-outline-secondary">
                                            <i class="bi bi-pencil-square"></i>
                                        </a>

                                    <?php endif ?>

                                    <?php if ($app->role->id == 3) : ?>
                                        <a href="/?page=task&id=<?= $task->id ?>" class="btn btn-sm btn-outline-secondary">
                                            <i class="bi bi-reply-fill"></i>
                                        </a>

                                    <?php endif ?>
                                </div>
                                <small class="text-muted"><?= $task->created_dt ?></small>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach ?>

        </div>
    </div>
</div>