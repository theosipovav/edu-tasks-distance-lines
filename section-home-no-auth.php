<div class="py-5 text-center container">
    <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <h1 class="fw-light">Добрый день</h1>
            <p class="lead text-muted">
                Вы находитесь на онлайн платформе по проверке остаточных знаний.
            </p>
            <p class="lead text-muted">
                Для продолжения работы вам необходимо выполнить вход под своими учетными данными или пройти процедуру регистрации.
            </p>
            <p>
                <a href="#" class="btn btn-lg btn-primary my-2" data-bs-toggle="modal" data-bs-target="#modalAuthorization">
                    <i class="bi bi-person-check-fill me-1"></i><span>Войти</span>
                </a>
                <a href="#" class="btn btn-lg btn-primary my-2" data-bs-toggle="modal" data-bs-target="#modalAuthorization">
                    <i class="bi bi-person-plus-fill me-1"></i><span>Регистрация</span>
                </a>
            </p>
        </div>
    </div>
</div>

<!-- Окно авторизации/регистрации -->
<div class="modal fade" id="modalAuthorization" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalAuthorizationLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAuthorizationLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-md-6 d-flex flex-column">
                        <h2 class="h2">Авторизация</h2>
                        <form id="FormLogIn" action="/" method="POST">
                            <div class="form-group">
                                <label for="InputLoginUsername">Логин</label>
                                <input type="text" name="username" class="form-control" id="InputLoginUsername" required title="Логин должен содержать не менее 3х символов и только латинские буквы в любом регистре.">
                            </div>
                            <div class="form-group">
                                <label for="InputLoginPassword">Пароль</label>
                                <input type="password" name="password" class="form-control" id="InputLoginPassword" required title="Пароль должен содержать не менее 3х символов">
                            </div>
                        </form>
                        <div class="flex-grow-1 mt-3"></div>
                        <hr>
                        <div class="d-flex flex-column">
                            <button type="submit" form="FormLogIn" name="action" value="form-login" class="btn btn-primary">Далее</button>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 d-flex flex-column">
                        <h2 class="h2">Регистрация</h2>
                        <form id="FormLogUp" action="/" method="POST">
                            <div class="form-group">
                                <label for="InputLogUpUsername">Логин</label>
                                <input type="text" name="username" class="form-control" id="InputLogUpUsername" required title="Логин должен содержать не менее 3х символов и только латинские буквы в любом регистре ">
                            </div>
                            <div class="form-row">
                                <div class="form-group">
                                    <label for="InputLogUpPassword">Пароль</label>
                                    <input type="password" name="password" class="form-control" id="InputLogUpPassword" required title="Пароль должен содержать не менее 3х символов">
                                </div>
                                <div class="form-group">
                                    <label for="InputLogUpPasswordRepeat">Повторите пароль</label>
                                    <input type="password" name="password-repeat" class="form-control" id="InputLogUpPasswordRepeat" required title="Пароль должен содержать не менее 3х символов">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="InputLogUpEmail">Адрес электронной почты</label>
                                <input type="email" name="email" class="form-control" id="InputLogUpEmail" placeholder="name@mail.ru" required>
                            </div>
                            <div class="form-group">
                                <label for="inputFormLogUpName">Ваше имя</label>
                                <input type="text" class="form-control" id="inputFormLogUpName" name="name" placeholder="Иванов Иван Иванович" required>
                            </div>
                            <div id="ContainerSelectFormLogUpRole" class="form-row">
                                <div class="col-12">
                                    <label for="SelectFormLogUpRole">Ваша роль</label>
                                    <select id="SelectFormLogUpRole" class="form-select" aria-label="Ваша учитель" name="role" required>
                                        <?php foreach ($app->db->getRoles(2) as $key => $role) : ?>
                                            <option value="<?= $role->id ?>"><?= $role->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="flex-grow-1 mt-3"></div>
                        <hr>
                        <div class="d-flex flex-column">
                            <button type="submit" form="FormLogUp" name="action" value="form-logup" class="btn btn-primary">Далее</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>