<?php
global $app;
$results = [];
if ($app->role->id == 3) {
    foreach ($app->db->getResults() as $key => $result) {
        if ($result->user_id == $app->user->id) {
            $results[] = $result;
        }
    }
} else {
    foreach ($app->db->getResults() as $key => $result) {
        $user = $app->db->getUserById($result->user_id);
        $task = $app->db->getTaskById($result->task_id);
        if (isset($app->filterTask['user'])) {
            if ($result->user_id != $app->filterTask['user']) {
                continue;
            }
        }
        if (isset($app->filterTask['status'])) {
            if ($app->filterTask['status'] == 1) {
                if ($result->res != $task->res) {
                    continue;
                }
            }
            if ($app->filterTask['status'] == 0) {
                if ($result->res == $task->res) {
                    continue;
                }
            }
        }
        $results[] = $result;
    }
}
?>
<div class="row">
    <div class="col">
        <h2 class="h2 mt-5">Результаты</h2>
    </div>
</div>
<?php if ($app->role->id == 2) : ?>

    <div class="row">
        <form id="FormFilterResult" action="<?= $_SERVER['REQUEST_URI'] ?>" class="mt-3" method="post">
            <div class="form-group d-flex ">
                <select class="form-select me-3 ms-3" name="user">
                    <option value="-1" disabled>Ученик</option>
                    <?php foreach ($app->db->getUsersByRole(3) as $key => $student) : ?>
                        <option value="<?= $student->id ?>"><?= $student->name ?> (<?= $student->id ?>)</option>
                    <?php endforeach ?>
                </select>

                <select class="form-select me-3 ms-3" name="status">
                    <option value="1">Решено правильно</option>
                    <option value="0">Решено с ошибкой</option>
                </select>
                <button type="submit" name="action" value="form-filter-result" class="btn btn-primary me-3 ms-3">Фильтр</button>
                <a href="<?= $app->url ?>" class="btn btn-primary me-3 ms-3">Сброс</a>

        </form>
    </div>
<?php endif ?>

<div class="row mt-5">
    <div class="col-md-1 d-flex justify-content-center align-items-center">
        <span>ID</span>
    </div>
    <div class="col-md-2 d-flex justify-content-start align-items-center">
        <span>Дата создания</span>
    </div>
    <div class="col-md-2 d-flex justify-content-center align-items-center">
        <span>Номер задачи</span>
    </div>
    <div class="col-md-2 d-flex flex-column justify-content-center align-items-center">
        <span>Ученик</span>
    </div>
    <div class="col-md-2 d-flex flex-column justify-content-center align-items-center">
        <span>Ответ ученика</span>
    </div>
    <div class="col-md-2 d-flex flex-column justify-content-center align-items-center">
        <span>Правильный ответ</span>
    </div>
    <div class="col-md-1 d-flex justify-content-center align-items-center">

    </div>
</div>
<?php foreach ($results as $key => $result) : ?>
    <?php
    $user = $app->db->getUserById($result->user_id);
    $task = $app->db->getTaskById($result->task_id);

    if ($result->res == $task->res) {
        $style = "style='background: rgba(76,175,80,0.5);'";
    } else {
        $style = "style='background: rgba(244,67,54,0.5);'";
    }

    ?>

    <div class="row pt-3 pb-3" <?= $style ?>>
        <div class="col-md-1 d-flex justify-content-center align-items-center">
            <?= $result->id ?>
        </div>
        <div class="col-md-2 d-flex justify-content-start align-items-center">
            <?= $result->created_dt ?>
        </div>
        <div class="col-md-2 d-flex justify-content-center align-items-center">
            <?= $task->id ?>
        </div>
        <div class="col-md-2 d-flex flex-column justify-content-center align-items-center">
            <div><?= $user->name ?></div>
        </div>
        <div class="col-md-2 d-flex flex-column justify-content-center align-items-center">
            <div><?= $result->res ?></div>
        </div>
        <div class="col-md-2 d-flex flex-column justify-content-center align-items-center">
            <div><?= $task->res ?></div>
        </div>
        <div class="col-md-1 d-flex justify-content-center align-items-center">
            <div class="btn-group">
                <a href="/?page=task&id=<?= $task->id ?>" class="btn btn-sm btn-secondary">Перейти</a>
            </div>
        </div>
    </div>
<?php endforeach ?>